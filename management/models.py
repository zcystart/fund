from django.db import models


class Fund(models.Model):
    fund_code = models.CharField(max_length=64, verbose_name='基金代码')
    fund_name = models.CharField(max_length=64, verbose_name='基金简称')
    update_time = models.CharField(max_length=128, verbose_name='更新日期')
    net_unit_value = models.CharField(max_length=64, verbose_name='单位净值')
    cumulative_net_value = models.CharField(max_length=64, verbose_name='累计净值')
    day_growth = models.CharField(max_length=64, verbose_name='日增长率')
    week = models.CharField(max_length=64, verbose_name='近一周')
    month = models.CharField(max_length=64, verbose_name='近一个月')
    three_months = models.CharField(max_length=64, verbose_name='近三个月')
    six_months = models.CharField(max_length=64, verbose_name='半年来')
    year = models.CharField(max_length=64, verbose_name='近一年')
    two_years = models.CharField(max_length=64, verbose_name='近两年')
    three_years = models.CharField(max_length=64, verbose_name='近三年')
    this_year = models.CharField(max_length=64, verbose_name='今年来')
    establish = models.CharField(max_length=64, verbose_name='成立以来')
    buy_fee_rate = models.CharField(max_length=64, verbose_name='手续费')

    def __str__(self):
        return self.fund_code


class Stock(models.Model):
    fund_code = models.CharField(max_length=64, verbose_name='基金代码')
    one = models.CharField(max_length=128, verbose_name='股票1', null=True)
    two = models.CharField(max_length=128, verbose_name='股票2', null=True)
    three = models.CharField(max_length=128, verbose_name='股票3', null=True)
    four = models.CharField(max_length=128, verbose_name='股票4', null=True)
    five = models.CharField(max_length=128, verbose_name='股票5', null=True)
    six = models.CharField(max_length=128, verbose_name='股票6', null=True)
    seven = models.CharField(max_length=128, verbose_name='股票7', null=True)
    eight = models.CharField(max_length=128, verbose_name='股票8', null=True)
    nine = models.CharField(max_length=128, verbose_name='股票9', null=True)
    ten = models.CharField(max_length=128, verbose_name='股票10', null=True)

    # fund = models.ForeignKey(Fund, on_delete=models.CASCADE, verbose_name='持仓前十股票')

    def __str__(self):
        return self.fund_code


class Rank_stock(models.Model):
    stock_code = models.CharField(max_length=128, verbose_name='股票代码')
    hold = models.CharField(max_length=32, verbose_name='机构持有数')

    def __str__(self):
        return self.stock_code
