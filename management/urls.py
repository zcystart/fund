from django.urls import path
from management import views

urlpatterns = [
    path('', views.index, name='homepage'),
    path('sign_up/', views.sign_up, name='sign_up'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('change_password/', views.change_password, name='change_password'),

    path('add_fund/', views.add_fund, name='add_fund'),
    # path('add_img/', views.add_img, name='add_img'),
    path('fund_list/<str:buy_fee_rate>/', views.fund_list, name='fund_list'),
    path('fund_detail/<int:fund_id>/', views.fund_detail, name='fund_detail'),
    path('users/', views.user_list, name='user_list')



]
